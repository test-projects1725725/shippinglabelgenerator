﻿namespace ShippingLabelGenerator.Models
{
    public class DomesticItem : ShippingItem
    {
        public bool IsRushDelivery { get; set; }
    }
}
