﻿using System.ComponentModel.DataAnnotations;

namespace ShippingLabelGenerator.Models
{
    public abstract class ShippingItem
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Sender name is required.")]
        public string SenderName { get; set; }

        [Required(ErrorMessage = "Sender address is required.")]
        public string SenderAddress { get; set; }

        [Required(ErrorMessage = "Recipient name is required.")]
        public string RecipientName { get; set; }

        [Required(ErrorMessage = "Recipient address is required.")]
        public string RecipientAddress { get; set; }

        [Required(ErrorMessage = "Weight is required.")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Weight must be greater than 0.")]
        public decimal Weight { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "Cost must be a non-negative value.")]
        public decimal? Cost { get; set; }

        [StringLength(20, ErrorMessage = "Tracking number must be at most 20 characters.")]
        public string? TrackingNumber { get; set; }
    }
}
