﻿using System.ComponentModel.DataAnnotations;
using Xunit.Abstractions;

namespace ShippingLabelGenerator.Models
{
    public class InternationalItem : ShippingItem
    {
        [StringLength(60, ErrorMessage = "Destination Country must be at most 60 characters.")]
        public string? DestinationCountry { get; set; }
        [StringLength(255, ErrorMessage = "Customs Declaration must be at most 255 characters.")]
        public string? CustomsDeclaration { get; set; }
    }
}
