﻿using Microsoft.EntityFrameworkCore;

namespace ShippingLabelGenerator.Models
{
    public class ShippingContext : DbContext
    {
        public ShippingContext(DbContextOptions<ShippingContext> options)
        : base(options)
        {
        }

        public DbSet<DomesticItem> DomesticItems { get; set; } = null!;
        public DbSet<InternationalItem> InternationalItems { get; set; } = null!;
        public DbSet<ShippingItem> ShippingItems { get; set; } = null!;
    }
}
