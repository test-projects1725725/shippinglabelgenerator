﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShippingLabelGenerator.Models;

namespace ShippingLabelGenerator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShippingItemsController : ControllerBase
    {
        private readonly ShippingContext _context;

        public ShippingItemsController(ShippingContext context)
        {
            _context = context;
        }

        // GET: api/ShippingItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShippingItem>>> GetShippingItems()
        {
            if (_context.ShippingItems == null)
            {
                return NotFound();
            }
            return await _context.ShippingItems.ToListAsync();
        }

        // GET: api/ShippingItems/5
        [HttpGet("id/{id}")]
        public async Task<ActionResult<ShippingItem>> GetShippingItem(long id)
        {
            if (_context.ShippingItems == null)
            {
                return NotFound();
            }
            var shippingItem = await _context.ShippingItems.FindAsync(id);

            if (shippingItem == null)
            {
                return NotFound();
            }

            return shippingItem;
        }

        // GET: api/ShippingItems/Scott
        [HttpGet("sender/{senderName}")]
        public ActionResult<IQueryable<ShippingItem>> GetShippingItemBySenderAsync(string senderName)
        {
            if (_context.ShippingItems == null)
            {
                return NotFound();
            }

            // Retrieve all shipping items for a given sender's name as IQueryable
            var shippingItems = _context.ShippingItems
                .Where(item => item.SenderName == senderName);

            if (!shippingItems.Any())
            {
                return NotFound();
            }

            return Ok(shippingItems);
        }
    }
}