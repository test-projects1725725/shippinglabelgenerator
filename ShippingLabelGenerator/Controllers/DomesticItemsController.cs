﻿using ShippingLabelGenerator.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShippingLabelGenerator.Models;

namespace ShippingLabelGenerator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DomesticItemsController : ControllerBase
    {
        private readonly ShippingContext _context;
        private readonly IHelperMethods _helperMethods;

        public DomesticItemsController(
            ShippingContext context,
            IHelperMethods helperMethods)
        {
            _context = context;
            _helperMethods = helperMethods;
        }

        // GET: api/DomesticItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DomesticItem>>> GetDomesticItems()
        {
            if (_context.DomesticItems == null)
            {
                return NotFound();
            }
            return await _context.DomesticItems.ToListAsync();
        }

        // GET: api/DomesticItems/5
        [HttpGet("id/{id}")]
        public async Task<ActionResult<DomesticItem>> GetDomesticItem(long id)
        {
            if (_context.DomesticItems == null)
            {
                return NotFound();
            }
            var domesticItem = await _context.DomesticItems.FindAsync(id);

            if (domesticItem == null)
            {
                return NotFound();
            }

            return domesticItem;
        }

        // POST: api/ShippingItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DomesticItem>> PostDomesticItem(DomesticItem domesticItem)
        {
            if (_context.ShippingItems == null)
            {
                return Problem("Entity set 'ShippingContext.DomesticItems'  is null.");
            }

            domesticItem.TrackingNumber = await _helperMethods.GetTrackingNumberAsync();
            domesticItem.Cost = await _helperMethods.CalculateCostAsync(domesticItem.Weight, domesticItem.IsRushDelivery, null);

            _context.DomesticItems.Add(domesticItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetDomesticItem), new { id = domesticItem.Id }, domesticItem);
        }

        private bool ShippingItemExists(long id)
        {
            return (_context.DomesticItems?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
