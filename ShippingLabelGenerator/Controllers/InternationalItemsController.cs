﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShippingLabelGenerator.Models;
using ShippingLabelGenerator.Business.Interfaces;

namespace ShippingLabelGenerator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InternationalItemsController : ControllerBase
    {
        private readonly ShippingContext _context;
        private readonly IHelperMethods _helperMethods;

        public InternationalItemsController(
            ShippingContext context,
            IHelperMethods helperMethods)
        {
            _context = context;
            _helperMethods = helperMethods;
        }

        // GET: api/InternationalItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InternationalItem>>> GetInternationalItems()
        {
            if (_context.InternationalItems == null)
            {
                return NotFound();
            }
            return await _context.InternationalItems.ToListAsync();
        }

        // GET: api/InternationalItems/5
        [HttpGet("id/{id}")]
        public async Task<ActionResult<InternationalItem>> GetInternationalItem(long id)
        {
            if (_context.InternationalItems == null)
            {
                return NotFound();
            }
            var internationalItem = await _context.InternationalItems.FindAsync(id);

            if (internationalItem == null)
            {
                return NotFound();
            }

            return internationalItem;
        }

        // POST: api/InternationalItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<InternationalItem>> PostInternationalItem(InternationalItem internationalItem)
        {
            if (_context.InternationalItems == null)
            {
                return Problem("Entity set 'ShippingContext.InternationalItems'  is null.");
            }

            internationalItem.TrackingNumber = await _helperMethods.GetTrackingNumberAsync();
            internationalItem.Cost = await _helperMethods.CalculateCostAsync(internationalItem.Weight, false, internationalItem.DestinationCountry);

            _context.InternationalItems.Add(internationalItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInternationalItem", new { id = internationalItem.Id }, internationalItem);
        }

        private bool InternationalItemExists(long id)
        {
            return (_context.InternationalItems?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
