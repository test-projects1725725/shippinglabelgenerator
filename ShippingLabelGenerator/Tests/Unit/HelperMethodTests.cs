﻿using Moq;
using Xunit;
using ShippingLabelGenerator.Business;

namespace ShippingLabelGenerator.Tests.Unit
{
    public class HelperMethodsTests
    {
        private readonly HelperMethods _helperMethods;
        private readonly IConfiguration _configuration;
        private readonly Mock<ILogger<HelperMethods>> _loggerMock;

        public HelperMethodsTests()
        {
            var configurationValues = new Dictionary<string, string>
            {
                { "ShippingConfig:StandardShippingRate", "10" },
                { "ShippingConfig:InternationalShippingFee", "5" },
                { "ShippingConfig:RushDeliveryFee", "3" }
            };

            _configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(configurationValues)
                .Build();

            _loggerMock = new Mock<ILogger<HelperMethods>>();

            _helperMethods = new HelperMethods(_configuration, _loggerMock.Object);
        }

        [Fact]
        public async Task GetTrackingNumberAsync_ReturnsValidTrackingNumber()
        {
            // Act
            var trackingNumber = await _helperMethods.GetTrackingNumberAsync();

            // Assert
            Assert.NotNull(trackingNumber);
            Assert.NotEmpty(trackingNumber);
        }

        [Theory]
        [InlineData(10, false, null, 100)] // Standard shipping
        [InlineData(5, true, null, 53)] // Rush delivery
        [InlineData(8, false, "Mexico", 85)] // International shipping
        public async Task CalculateCostAsync_ReturnsCorrectCost(decimal weight, bool isRushDelivery, string destinationCountry, decimal expectedCost)
        {
            // Act
            var cost = await _helperMethods.CalculateCostAsync(weight, isRushDelivery, destinationCountry);

            // Assert
            Assert.Equal(expectedCost, cost);
        }

        [Fact]
        public async Task CalculateCostAsync_ThrowsException_WhenUsingUSAsInternationalShipmentDestination()
        {
            // Arrange
            string destinationCountry = "US";

            // Act
            var exception = await Assert.ThrowsAsync<Exception>(() => _helperMethods.CalculateCostAsync(10, false, destinationCountry));

            // Assert
            Assert.Equal("Cannot use the US as an international shipment destination.", exception.Message);
        }
    }
}