﻿using ShippingLabelGenerator.Business.Interfaces;

namespace ShippingLabelGenerator.Business
{
    public class HelperMethods : IHelperMethods
    {
        private readonly decimal _standardShippingRate;
        private readonly decimal _internationalShippingFee;
        private readonly decimal _rushDeliveryFee;
        private readonly ILogger<HelperMethods> _logger;

        public HelperMethods(IConfiguration configuration, ILogger<HelperMethods> logger)
        {
            _standardShippingRate = configuration.GetSection("ShippingConfig").GetValue<decimal>("StandardShippingRate");
            _internationalShippingFee = configuration.GetSection("ShippingConfig").GetValue<decimal>("InternationalShippingFee");
            _rushDeliveryFee = configuration.GetSection("ShippingConfig").GetValue<decimal>("RushDeliveryFee");
            _logger = logger;
        }

        public async Task<string> GetTrackingNumberAsync()
        {
            string trackingNumber = Guid.NewGuid().ToString();

            if (string.IsNullOrEmpty(trackingNumber))
            {
                _logger.LogError("Tracking number generation failed.");
                throw new Exception("Tracking number generation failed.");
            }

            _logger.Log(LogLevel.Information, $"New tracking number generated: {trackingNumber}");

            return await Task.FromResult(trackingNumber);
        }

        public Task<decimal> CalculateCostAsync(decimal weight, bool isRushDelivery, string destinationCountry)
        {

            decimal cost;
            decimal standardShippingRate = _standardShippingRate;

            string[] excludedCountries = { "US", "USA", "United States", "United States of America" };

            // Check if the country is not the US, which means no rush delivery
            // charge applies and an international shipping fee is applied. If the country is
            // US, then check for rush delivery and add the appropriate fee.
            if (!excludedCountries.Contains(destinationCountry) && !string.IsNullOrEmpty(destinationCountry))
            {
                var internationalShippingFee = _internationalShippingFee;
                cost = weight * standardShippingRate + internationalShippingFee;
            }
            else if (isRushDelivery)
            {
                var rushDeliveryFee = _rushDeliveryFee;
                cost = weight * standardShippingRate + rushDeliveryFee;
            }
            else
            {
                cost = weight * standardShippingRate;
            }

            if (excludedCountries.Contains(destinationCountry))
            {
                _logger.LogError("Cannot use the US as an international shipment destination.");
                throw new Exception("Cannot use the US as an international shipment destination.");
            }

            _logger.LogInformation("Adding cost for shipment.");

            return Task.FromResult(cost);
        }
    }
}
