﻿namespace ShippingLabelGenerator.Business.Interfaces
{
    public interface IHelperMethods
    {
        /// <summary>
        /// Generates a unique tracking number for the shipment
        /// </summary>
        /// <returns>A tracking number.</returns>
        Task<string> GetTrackingNumberAsync();

        /// <summary>
        /// Calculates cost of the shipment using weight, service type, and destination country.
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="isRushDelivery"></param>
        /// <param name="destinationCountry"></param>
        /// <returns>Cost for the shipment.</returns>
        Task<decimal> CalculateCostAsync(decimal weight, bool isRushDelivery, string destinationCountry);
    }
}
